export const FORM_ERRORS = {
  isLetter: 'must contain only alphabets',
  isRequired: 'is required',
  isEmail: 'is invalid',
  isPhone: 'is invalid',
  isPassword: 'must contain small letter, capital letter, number, special character and 8 characters'
}