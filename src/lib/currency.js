export const CURRENCY_ICON_MAP = {
  INR: 'rupee sign',
  DOLLAR: 'dollar sign',
  EURO: 'euro sign',
  POUND: 'pound sign',
};