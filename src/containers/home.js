import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  fetchRestaurant,
  fetchOrders,
  unsubscribeOrders,
  fetchTables,
  unsubscribeTables,
  addUser,
  updateOrderStatus,
  fetchPayments,
  unsubscribePayments,
  fetchReviews,
  unsubscribeReviews,
  getUserDetails,
} from '../actions/home';

const STATUS_TEXT_MAP = {
  '1': 'being delivered',
  '0': 'confirmed',
  '-1': 'cancelled'
}

class Home extends Component {
  state = {
    subscribeOrders: false,
    subscribeTables: false,
    subscribePayments: false,
    subscribeReviews: false,
  }

  componentDidMount() {
    const { dispatch, user } = this.props;
    dispatch(fetchRestaurant(user));
  }

  componentDidUpdate() {
    const { subscribeOrders, subscribeTables, subscribePayments, subscribeReviews } = this.state;
    const { restaurant, dispatch } = this.props;
    if (restaurant.id && !subscribeOrders) {
      dispatch(fetchOrders(restaurant.id));
      this.setState({
        subscribeOrders: true,
      });
    }
    if (restaurant.id && !subscribeTables) {
      dispatch(fetchTables(restaurant.id));
      this.setState({
        subscribeTables: true,
      });
    }
    if (restaurant.id && !subscribePayments) {
      dispatch(fetchPayments(restaurant.id));
      this.setState({
        subscribePayments: true,
      });
    }
    if (restaurant.id && !subscribeReviews) {
      dispatch(fetchReviews(restaurant.id));
      this.setState({
        subscribeReviews: true,
      });
    }
  }

  componentWillUnmount() {
    unsubscribeOrders();
    unsubscribeTables();
    unsubscribePayments();
    unsubscribeReviews();
  }

  addUser = (data) => {
    const { dispatch } = this.props;
    dispatch(addUser(data));
  }

  updateOrderStatus = (order) => {
    getUserDetails(order, this.onFetchUserSuccess);
  }

  onFetchUserSuccess = (order, user) => {
    const { dispatch } = this.props;
    const smsData = {
      send: !!user.phoneNumber,
      receiver: user.phoneNumber,
      message: `Your order with id ${order.id} is ${STATUS_TEXT_MAP[order.statusCode]}`
    };
    dispatch(updateOrderStatus(order, smsData));
  }

  render() {
    const { Layout, restaurant, user, orders, tables, userAdded, payments, reviews } = this.props;

    return (
      <Layout
        restaurant={restaurant}
        user={user}
        orders={orders}
        tables={tables}
        reviews={reviews}
        payments={payments}
        addUser={this.addUser}
        userAdded={userAdded}
        updateStatus={this.updateOrderStatus}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  restaurant: state.restaurant.restaurant,
  orders: state.restaurant.orders || [],
  tables: state.restaurant.tables || [],
  userAdded: state.restaurant.userAdded,
  payments: state.restaurant.payments,
  reviews: state.restaurant.reviews,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);