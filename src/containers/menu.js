import React, { Component } from 'react';
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { fetchRestaurant } from '../actions/home';
import { fetchMenuItems, unsubscribeMenu, updateMenu, fetchFoodCategories, updateCategory, unsubscribeFoodCategories, deleteCategory } from '../actions/menu';

class Menu extends Component {
  state = {
    subscribeMenu: false,
    subscribeCategories: false,
  }

  componentDidMount() {
    const { dispatch, user } = this.props;
    dispatch(fetchRestaurant(user));
  }

  componentDidUpdate() {
    const { subscribeMenu, subscribeCategories } = this.state;
    const { restaurant, dispatch } = this.props;
    if (restaurant.id && !subscribeMenu) {
      dispatch(fetchMenuItems(restaurant.id));
      this.setState({
        subscribeMenu: true,
      });
    }
    if (restaurant.id && !subscribeCategories) {
      dispatch(fetchFoodCategories(restaurant.id));
      this.setState({
        subscribeCategories: true,
      });
    }
  }

  updateMenu = (data) => {
    const { dispatch, menu = [], menuId, restaurant } = this.props;
    const newMenu = menu.map(menuItem => Object.assign({}, menuItem));
    const menuItemIndex = newMenu.findIndex(menuItem => menuItem.id === data.id);
    if (menuItemIndex !== -1) {
      const targetMenu = newMenu[menuItemIndex];
      newMenu[menuItemIndex] = Object.assign({}, targetMenu, data);
    } else {
      newMenu.push(data);
    }

    const restaurantMenu = Object.assign({}, {
      items: newMenu,
      restaurantId: restaurant.id,
      id: menuId || uuidv4(),
    });

    const action = menuId ? 'update' : 'set';
    dispatch(updateMenu(restaurantMenu, action));
  }

  deleteItem = (id) => {
    const { dispatch, menu = [], menuId, restaurant } = this.props;
    const newMenu = menu
      .map(menuItem => Object.assign({}, menuItem))
      .filter(menuItem => menuItem.id !== id);

    const restaurantMenu = Object.assign({}, {
      items: newMenu,
      restaurantId: restaurant.id,
      id: menuId,
    });
    dispatch(updateMenu(restaurantMenu, 'update'));
  }

  componentWillUnmount() {
    unsubscribeMenu();
    unsubscribeFoodCategories();
  }

  updateCategory = (data, action) => {
    const { dispatch } = this.props;
    dispatch(updateCategory(data, action));
  }

  deleteCategory = (data) => {
    const { dispatch } = this.props;
    dispatch(deleteCategory(data));
  }

  render() {
    const { Layout, restaurant, user, menu, menuAdded, menuDeleted, categories } = this.props;

    return (
      <Layout
        restaurant={restaurant}
        user={user}
        menu={menu}
        categories={categories}
        updateMenu={this.updateMenu}
        menuAdded={menuAdded}
        deleteItem={this.deleteItem}
        menuDeleted={menuDeleted}
        updateCategory={this.updateCategory}
        deleteCategory={this.deleteCategory}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  restaurant: state.restaurant.restaurant,
  menu: state.restaurant.menu,
  menuId: state.restaurant.menuId,
  menuAdded: state.restaurant.menuAdded,
  menuDeleted: state.restaurant.menuDeleted,
  categories: state.restaurant.categories,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);