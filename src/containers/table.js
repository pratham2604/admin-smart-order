import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRestaurant, fetchOrders, unsubscribeOrders, fetchTables, unsubscribeTables, addTable } from '../actions/home';

class Home extends Component {
  state = {
    subscribeOrders: false,
    subscribeTables: false,
  }

  componentDidMount() {
    const { dispatch, user } = this.props;
    dispatch(fetchRestaurant(user));
  }

  componentDidUpdate() {
    const { subscribeTables, subscribeOrders } = this.state;
    const { restaurant, dispatch } = this.props;
    if (restaurant.id && !subscribeOrders) {
      dispatch(fetchOrders(restaurant.id));
      this.setState({
        subscribeOrders: true,
      });
    }
    if (restaurant.id && !subscribeTables) {
      dispatch(fetchTables(restaurant.id));
      this.setState({
        subscribeTables: true,
      });
    }
  }

  addTable = (data) => {
    const { dispatch } = this.props;
    dispatch(addTable(data));
  }

  componentWillUnmount() {
    unsubscribeOrders();
    unsubscribeTables();
  }

  render() {
    const { Layout, restaurant, user, tables, orders } = this.props;

    return (
      <Layout
        restaurant={restaurant}
        user={user}
        tables={tables}
        orders={orders}
        addTable={this.addTable}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  restaurant: state.restaurant.restaurant,
  tables: state.restaurant.tables || [],
  orders: state.restaurant.orders || [],
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);