import Config from '../config';
import { TEMPLATE_TYPES } from '../templates/index';
import HomeContainer from '../../containers/home';
import HomeComponent from '../../modules/home/index';

export default {
  Component: HomeComponent,
  Container: HomeContainer,
  path: '/',
  exact: true,
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.AUTH_SIDEBAR,
};