import Config from '../config';
import { TEMPLATE_TYPES } from '../templates/index';
import MenuContainer from '../../containers/menu';
import MenuComponent from '../../modules/menu/index';

export default {
  Component: MenuComponent,
  Container: MenuContainer,
  path: '/menu',
  exact: true,
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.AUTH_SIDEBAR,
};