export default {
  fetching: false,
  error: null,
  user: {},
  isAuthenticated: false,
  token: null,
};