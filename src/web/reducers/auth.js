import Store from '../store/auth';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_AUTH_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        isAuthenticated: false,
      });
    case TYPES.FETCH_AUTH_SUCCESS:
      return Object.assign({}, state, {
        user: action.data,
        fetching: false,
        token: action.token,
        error: null,
        isAuthenticated: true,
        signingIn: false,
      });
    case TYPES.FETCH_AUTH_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        isAuthenticated: false,
        user: {},
        token: null,
        signingIn: false,
      });

    case TYPES.SIGNUP_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        isAuthenticated: false,
        signingIn: true,
      });
    case TYPES.SIGNUP_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        signingIn: false,
      });

    case TYPES.LOGIN_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        signingIn: true,
      });
    case TYPES.LOGIN_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        signingIn: false,
      });

    case TYPES.SIGNOUT_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
      });
    default:
      return state;
  }
}