import Store from '../store/restaurant';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function restaurantReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_RESTAURANT_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: '',
      });
    case TYPES.FETCH_RESTAURANT_SUCCESS:
      return Object.assign({}, state, {
        restaurant: action.data,
        fetching: false,
        error: '',
      });
    case TYPES.FETCH_RESTAURANT_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        restaurant: {},
      });

    case TYPES.FETCH_MENU_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: '',
      });
    case TYPES.FETCH_MENU_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        menu: action.data,
        menuId: action.menuId,
      });

    case TYPES.ADD_MENU_REQUEST:
      return Object.assign({}, state, {
        menuAdded: false,
      });
    case TYPES.ADD_MENU_SUCCESS:
      return Object.assign({}, state, {
        menuAdded: true,
      });

    case TYPES.DELETE_MENU_REQUEST:
      return Object.assign({}, state, {
        menuDeleted: false,
      });
    case TYPES.DELETE_MENU_SUCCESS:
      return Object.assign({}, state, {
        menuDeleted: true,
      });

    case TYPES.FETCH_ORDERS_REQUEST:
      return Object.assign({}, state, {
        fetchingOrders: true,
        error: '',
      });
    case TYPES.FETCH_ORDERS_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetchingOrders: false,
        orders: action.data,
      });

    case TYPES.FETCH_TABLES_REQUEST:
      return Object.assign({}, state, {
        fetchingTables: true,
        error: '',
      });
    case TYPES.FETCH_TABLES_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetchingTables: false,
        tables: action.data,
      });

    case TYPES.ADD_USER_REQUEST:
      return Object.assign({}, state, {
        userAdded: false,
      });
    case TYPES.ADD_USER_SUCCESS:
      return Object.assign({}, state, {
        userAdded: true,
      });

    case TYPES.FETCH_PAYMENTS_REQUEST:
      return Object.assign({}, state, {
        fetchingPayments: true,
        error: '',
      });
    case TYPES.FETCH_PAYMENTS_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetchingPayments: false,
        payments: action.data,
      });

    case TYPES.FETCH_REVIEWS_REQUEST:
      return Object.assign({}, state, {
        fetchingReviews: true,
        error: '',
      });
    case TYPES.FETCH_REVIEWS_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetchingReviews: false,
        reviews: action.data,
      });

    case TYPES.FETCH_MENU_CATEGORIES_REQUEST:
      return Object.assign({}, state, {
        fetchingMenuCategories: true,
        error: '',
      });
    case TYPES.FETCH_MENU_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        error: action.message,
        fetchingMenuCategories: false,
        categories: action.data,
      });

    case TYPES.CLEAR_RESTAURANT_DATA:
      return Object.assign({}, state, {
        restaurant: {},
        menu: [],
        reviews: [],
        categories: [],
        payments: [],
        tables: [],
        orders: []
      });

    default:
      return state;
  }
}