import React, { Component, Fragment } from 'react';
import DataTable from '../../../../components/UI/datatable/datatable';
import { Icon } from 'semantic-ui-react';
import OptionModal from './optionModal';

const columns = [{
  title: 'Name',
  field: 'name',
}, {
  title: 'Ingredients',
  field: 'ingredients',
}, {
  title: 'Price',
  field: 'price',
}, {
  title: 'Availability',
  field: 'available',
}, {
  title: 'Action',
  field: 'actions',
}];

export default class OptionsTable extends Component {
  render() {
    const { data, onDelete, updateOptions } = this.props;
    const rows = data.map((option, index) => {
      return Object.assign({
        name: option.name,
        ingredients: option.ingredients,
        price: option.price,
        available: option.available ? 'Yes' : 'No',
        actions: (
          <Fragment>
            <OptionModal data={option} edit index={index} updateOptions={updateOptions} />
            <Icon name="trash" className="action-icon" color="red" onClick={onDelete.bind(this, index)} />
          </Fragment>
        )
      })
    });
    const table = { columns, rows };
    return (
      <div className="option-table-container">
        <DataTable data={table} />
      </div>
    )
  }
}