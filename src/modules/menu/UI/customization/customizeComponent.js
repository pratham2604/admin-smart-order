import React, { Component } from 'react';
import { Segment, Grid, Button } from 'semantic-ui-react';
import FC from '../../../../components/formsy/index';
import OptionModal from './optionModal';
import OptionsTable from './optionsTable';

export default class CustomizeComponent extends Component {
  constructor(props) {
    super(props);
    const { data = {} } = this.props;
    const {
      showOptions = false,
      name = '',
      ingredients = '',
      price = 0,
      options = [],
      optionsSelectType = 'multiselect',
      requirement = 'required',
      isAvailable = true,
    } = data;
    this.state = {
      name,
      ingredients,
      isAvailable,
      price,
      showOptions,
      options,
      optionsSelectType,
      requirement,
      canEdit: false,
    }
  }

  onChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    });
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  onSubmit = () => {
    const { addCustomization, index } = this.props;
    const { options, name, requirement, ingredients, price, showOptions, optionsSelectType, isAvailable } = this.state;
    const data = Object.assign({}, {
      name,
      requirement,
      ingredients,
      isAvailable,
      price,
      showOptions,
      optionsSelectType,
      options,
    });
    addCustomization(data, index);
  }

  updateOptions = (option, index) => {
    const { options } = this.state;
    const updatedOptions = options.map(option => Object.assign({}, option));
    if (index !== -1) {
      updatedOptions[index] = option;
    } else {
      updatedOptions.push(option);
    }
    this.setState({
      options: updatedOptions
    })
  }

  onDelete = (index) => {
    const { options } = this.state;
    const updatedOptions = options.map(option => Object.assign({}, option));
    updatedOptions.splice(index);
    this.setState({
      options: updatedOptions,
    });
  }

  render() {
    const { showOptions, options, name, requirement, ingredients, price, optionsSelectType, isAvailable } = this.state;
    const { onClose } = this.props;
    const types = [{
      label: 'Required',
      value: 'required',
    }, {
      label: 'Optional',
      value: 'optional'
    }];

    const selectTypes = [{
      label: 'MultiSelect',
      value: 'multiselect',
    }, {
      label: 'SingleSelect',
      value: 'singleselect',
    }]

    return (
      <Segment className="customisation-component">
        <Grid stackable>
          <Grid.Row>
            <Grid.Column width={9}>
              <FC.Input fluid placeholder='Name' name="name" required value={name} onChange={this.onChange}/>
            </Grid.Column>
            <Grid.Column width={5}>
              <FC.RadioButtons name="requirement" options={types} value={requirement} showBorder={false} onChange={this.onChange}/>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <FC.Input fluid placeholder='Ingredients' name="ingredients" required value={ingredients} onChange={this.onChange}/>
              <FC.Checkbox label="Available" name="isAvailable" toggle value={isAvailable} required onChange={this.onChange}/>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={6}>
              <FC.Checkbox label="Provide Options" name="showOptions" toggle value={showOptions} required onChange={this.onChange}/>
            </Grid.Column>
            {showOptions ?
              <Grid.Column width={10}>
                <FC.RadioButtons name="optionsSelectType" options={selectTypes} value={optionsSelectType} showBorder={false} onChange={this.onChange}/>
              </Grid.Column> :
              <Grid.Column width={10}>
                <FC.Input fluid placeholder='Price' name="price" required value={price} onChange={this.onChange} />
              </Grid.Column>
            }
          </Grid.Row>
          {showOptions &&
            <Grid.Row>
              <Grid.Column>
                <div>
                  <OptionsTable data={options} updateOptions={this.updateOptions} onDelete={this.onDelete}/>
                  <OptionModal updateOptions={this.updateOptions} index={-1}/>
                </div>
              </Grid.Column>
            </Grid.Row>
          }
          <Grid.Row>
            <Grid.Column width={16}>
              <Button negative onClick={onClose}>Cancel</Button>
              <Button type="button" positive onClick={this.onSubmit}>Save</Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )
  }
}