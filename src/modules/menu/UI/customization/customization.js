import React, { Component, Fragment } from 'react';
import { Segment, Header, Button, Icon } from 'semantic-ui-react';
import CustomizeComponent from './customizeComponent';
import DataTable from '../../../../components/UI/datatable/datatable';

const columns = [{
  title: 'Name',
  field: 'name',
}, {
  title: 'Ingredients',
  field: 'ingredients',
}, {
  title: 'Requirement',
  field: 'requirement',
}, {
  title: 'Availability',
  field: 'available',
}, {
  title: 'Action',
  field: 'actions',
}];

export default class Customization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      activeIndex: -1,
    }
  }

  toggle = (index) => {
    const { open } = this.state;
    const newIndex = index > -1 ? index : -1;
    this.setState({
      open: !open,
      activeIndex: open ? -1 : newIndex
    });
  }

  addCustomization = (data, index) => {
    const { customizations, updateCustomizations } = this.props;
    const newCustomizations = customizations.map(cstm => Object.assign({}, cstm));
    if (index !== -1) {
      newCustomizations[index] = data;
    } else {
      newCustomizations.push(data);
    }
    updateCustomizations(newCustomizations);
    this.setState({
      open: false,
      activeIndex: -1,
    });
  }

  render() {
    const { customizations } = this.props;
    const rows = customizations.map((option, index) => {
      return Object.assign({}, {
        name: option.name,
        ingredients: option.ingredients,
        available: option.isAvailable ? 'Yes' : 'No',
        requirement: option.requirement,
        actions: (
          <Fragment>
            <Icon name="pencil" className="action-icon" onClick={this.toggle.bind(this, index)}></Icon>
            <Icon name="trash" className="action-icon" color="red" />
          </Fragment>
        )
      })
    });
    const table = { columns, rows };
    const { open, activeIndex } = this.state;
    const data = activeIndex === -1 ? {} : customizations[activeIndex];

    return (
      <Segment className="customisations-container">
        <Header as="h3">Customizations</Header>
        <div className="customisation-table">
          <DataTable data={table} />
        </div>
        {open ?
          <CustomizeComponent onClose={this.toggle} addCustomization={this.addCustomization} data={data} index={activeIndex}/> :
          <Button type="button" positive onClick={this.toggle}>Add New Customization</Button>
        }
      </Segment>
    )
  }
}