import React, { Component, Fragment } from 'react';
import { Segment, Header, Grid, Button, Modal, Icon } from 'semantic-ui-react';
import FC from '../../../../components/formsy/index';

export default class OptionModal extends Component {
  constructor(props) {
    super(props);
    const { name = '', ingredients = '', price = 0, available = true } = props.data || {};
    this.state = {
      canEdit: false,
      name,
      ingredients,
      price,
      available,
      modalOpen: false
    };
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  componentDidUpdate(prevProps) {
    const { name, ingredients, price, available } = this.props;
    if (prevProps.name !== name || prevProps.ingredients !== ingredients || prevProps.price !== price || prevProps.available !== available) {
      this.setState({
        name,
        ingredients,
        price,
        available,
      });
    }
  }

  onChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

  onSubmit = () => {
    const { updateOptions, index } = this.props;
    const { name, ingredients, price, available } = this.state;
    const option = Object.assign({}, {
      name,
      ingredients,
      price: parseInt(price),
      available,
    });
    updateOptions(option, index);
    this.setState({
      modalOpen: false,
      name: '',
      ingredients: '',
      price: 0,
      available: true,
    })
  }

  render() {
    const { onClose, edit = false } = this.props;
    const { name, ingredients, price, available, modalOpen } = this.state;
    const trigger = <Tigger edit={edit} handleOpen={this.handleOpen} />;

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="tiny" closeIcon>
        <Modal.Content>
          <Header as="h3">Customization Option</Header>
          <Segment>
            <Grid>
              <Grid.Row>
                <Grid.Column width={12}>
                  <FC.Input fluid placeholder='Name' name="name" required value={name} onChange={this.onChange}/>
                </Grid.Column>
                <Grid.Column width={4}>
                  <FC.Input fluid placeholder='Price' name="price" required value={price} onChange={this.onChange} type="number"/>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <FC.Input fluid placeholder='Ingredients' name="ingredients" required value={ingredients} onChange={this.onChange}/>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <FC.Checkbox label="Availability" name="available" toggle value={available} required onChange={this.onChange}/>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  <Button type="button" negative onClick={onClose}>Cancel</Button>
                  <Button type="button" positive onClick={this.onSubmit}>Save</Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </Modal.Content>
      </Modal>
    )
  }
}

const Tigger = (props) => {
  const { edit, handleOpen } = props;
  return (
    <Fragment>
      {edit ?
        <Icon name="pencil" onClick={handleOpen} style={{cursor: 'pointer'}}/> :
        <Button type="button" color="blue" className="add-menu" onClick={handleOpen}>Add New Option</Button>
      }
    </Fragment>
  )
}