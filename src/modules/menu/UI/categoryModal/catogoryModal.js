import React, { Component, Fragment } from 'react';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';
import { Button, Header, Icon, Modal, Grid } from 'semantic-ui-react';
import FC from '../../../../components/formsy/index';
import DataTable from '../../../../components/UI/datatable/datatable';
// import Customization from '../customization/customization';
// import { CURRENCY_ICON_MAP } from '../../../../lib/currency';

const columns = [{
  title: 'Name',
  field: 'name',
  sort: true,
}, {
  title: 'Action',
  field: 'actions',
}]

export default class CategoryModal extends Component {
  state = {
    modalOpen: false,
    canEdit: false,
    submitted: false,
    category: {},
  }

  toggleModal = () => {
    this.setState({
      modalOpen: !this.state.modalOpen,
    });
  }

  reset = () => {
    this.setState({
      category: {},
    });
  }

  selectCategory = (category) => {
    this.setState({
      category: Object.assign({}, category)
    });
  }

  render() {
    const { categories = [], restaurant, update, deleteCategory } = this.props;
    const { modalOpen, category } = this.state;
    const trigger = <Button color="blue" className="add-menu" onClick={this.toggleModal}>Manage Categories</Button>;
    const rows = categories.map(category => {
      const { name } = category;
      return Object.assign({}, {
        name,
        actions: (
          <Fragment>
            <Icon name="pencil" onClick={this.selectCategory.bind(this, category)} style={{cursor: 'pointer'}}/>
            <Icon name="close" color="red" onClick={deleteCategory.bind(this, category)} style={{cursor: 'pointer', marginLeft: '1em'}}/>
          </Fragment>
        )
      })
    });
    const table = { columns, rows };

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.toggleModal} centered={false} size="tiny" closeIcon>
        <Modal.Content>
          <Header as="h1">
            Manage Categories
          </Header>
          <DataTable data={table} />
          <EditCategory category={category} update={update} restaurant={restaurant} reset={this.reset}/>
        </Modal.Content>
      </Modal>
    )
  }
}

class EditCategory extends Component {
  state = {
    canEdit: false,
    showButton: true,
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  onSubmit = (model) => {
    const { category, update, restaurant, reset } = this.props;
    const data = Object.assign({}, model, {
      id: category.id || uuidv4(),
      restaurantId: restaurant.id,
      value: (model.name || '').toLowerCase().split(' ').join('-'),
    });
    const action = category.id ? 'update' : 'set';
    update(data, action);
    this.setState({
      showButton: true,
    });
    reset();
  }

  showForm = () => {
    this.setState({
      showButton: false,
    });
  }

  hideForm = () => {
    this.setState({
      showButton: true,
    });
    this.props.reset();
  }

  render() {
    const { category } = this.props;
    const { name = '' } = category;
    const { canEdit, showButton } = this.state;

    return (
      <div className="edit-category-container">
        <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit}>
          {!name && showButton && <Button type="button" onClick={this.showForm}>Add</Button>}
          {(name || !showButton) &&
            <Fragment>
              <FC.Input fluid placeholder='Name' name="name" required value={name}/>
              <FC.SubmitButton disabled={!canEdit} positive>Save</FC.SubmitButton>
              <Button type="button" negative>Cancel</Button>
            </Fragment>
          }
        </FC.Form>
      </div>
    )
  }
}
