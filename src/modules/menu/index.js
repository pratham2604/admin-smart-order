import React, { Component } from 'react';
import { Segment, Header } from 'semantic-ui-react';
import DataTable from '../../components/UI/datatable/datatable';
import ImageHolder from '../../components/UI/imageHolder/imageHolder';
import MenuModal from './UI/menuModal/menuModal';
import CategoryModal from './UI/categoryModal/catogoryModal';
import FC from '../../components/formsy/index';

const columns = [{
  title: 'Photo',
  field: 'photo',
}, {
  title: 'Name',
  field: 'name',
  sort: true,
}, {
  title: 'Price',
  field: 'price',
  sort: true,
}, {
  title: 'Availability',
  field: 'available',
  sort: true,
}, {
  title: 'Action',
  field: 'actions',
}]

export default class extends Component {
  state = {
  }

  onChange = (menu, name, value) => {
    const { updateMenu } = this.props;
    const updatedMenu = Object.assign({}, menu, {
      isAvailable: value,
    });
    updateMenu(updatedMenu);
  }

  render() {
    const { menu = [], updateMenu, menuAdded, menuDeleted, deleteItem, categories, restaurant, updateCategory, deleteCategory } = this.props;
    const rows = menu.map(menuItem => {
      const { thumbnail, name, price, isAvailable, ingredient } = menuItem;
      return Object.assign({}, {
        photo: <ImageHolder src={thumbnail} size="tiny" style={{width: '5em', borderRadius: 5}}/>,
        name,
        available: (<FC.Checkbox name="isAvailable" toggle value={isAvailable} required onChange={this.onChange.bind(this, menuItem)}/>),
        price,
        actions: (<MenuModal data={menuItem} edit={true} updateMenu={updateMenu} menuAdded={menuAdded} deleteItem={deleteItem} menuDeleted={menuDeleted} categories={categories} restaurant={restaurant}/>)
      })
    });
    const table = { columns, rows };
    return (
      <Segment className="menu-items-container">
        <Header as="h1">
          Menu
          <MenuModal updateMenu={updateMenu} menuAdded={menuAdded} deleteItem={deleteItem} menuDeleted={menuDeleted} categories={categories} restaurant={restaurant}/>
          <CategoryModal update={updateCategory} deleteCategory={deleteCategory} categories={categories} restaurant={restaurant}/>
        </Header>
        <FC.Form>
          <DataTable data={table} />
        </FC.Form>
      </Segment>
    )
  }
}