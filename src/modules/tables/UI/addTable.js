import React, { Component } from 'react';
import { Button, Header, Modal, Grid } from 'semantic-ui-react';
import FC from '../../../components/formsy/index';
const crypto = require('crypto');

export default class AddRoomModal extends Component {
  state = {
    modalOpen: false,
    canEdit: false,
    submitted: false,
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true,
      roomId: crypto.randomBytes(3).toString('hex').toUpperCase()
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  onSubmit = (model) => {
    const data = Object.assign({}, model, {
      id: this.state.roomId,
      restaurantId: this.props.restaurantId,
    });
    this.props.addTable(data);
    this.setState({
      submitted: true,
      modalOpen: false,
    })
  }

  render() {
    const { data, type } = this.props;
    const { modalOpen, canEdit, roomId } = this.state;
    const trigger = <Button color="blue" className="add-table" onClick={this.handleOpen}>Add {type}</Button>;
    const { number } = data || {};
    const placeholder = `${type} Number`;

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="mini" closeIcon>
        <Modal.Content>
          <Header as="h1">
            Add {type}
          </Header>
          <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit} validations={this.validations}>
            <Grid stackable columns={2} className="menu-container">
              <Grid.Row>
                <Grid.Column width={16}>
                  <FC.Input fluid placeholder={placeholder} name="number" required value={number}/>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  {type} ID: <Header as="h3">{roomId}</Header>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <FC.SubmitButton disabled={!canEdit}>Save</FC.SubmitButton>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </FC.Form>
        </Modal.Content>
      </Modal>
    )
  }
}