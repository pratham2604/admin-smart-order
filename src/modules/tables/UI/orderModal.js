import React, { Component } from 'react';
import { Button, Header, Modal, Divider } from 'semantic-ui-react';
const PENDING_STATUS = -2;
const CONFIRMED_STATUS = 0;
const DELIVERED_STATUS = 1;

export default class QRModal extends Component {
  state = {
    modalOpen: false,
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  getOrder = (itemOrders) => {
    const dishes = [];
    let amount = 0;
    itemOrders.forEach(itemOrder => {
      const { name, price } = itemOrder;
      const isDishPresent = dishes.find(dish => dish === name);
      if (!isDishPresent) {
        dishes.push(name);
      }
      amount += price;
    });
    const dishesContent = dishes.join(',');
    return {
      content: dishesContent,
      amount,
    };
  }

  render() {
    const { modalOpen } = this.state;
    const { table, orders = [] } = this.props;
    const trigger = <Button color="instagram" onClick={this.handleOpen}>Show Info</Button>;
    const orderForTable = orders.filter(order => order.tableId === table.id);
    const pendingOrders = orderForTable
      .filter(order => order.statusCode === PENDING_STATUS)
      .map(order => this.getOrder(order.itemOrders));
    const pendingTotal = pendingOrders.reduce((acc, value) => value.amount + acc, 0);

    const confirmedOrders = orderForTable
      .filter(order => order.statusCode === CONFIRMED_STATUS)
      .map(order => this.getOrder(order.itemOrders));
    const confirmedTotal = confirmedOrders.reduce((acc, value) => value.amount + acc, 0);

    const deliveredOrders = orderForTable
      .filter(order => order.statusCode === DELIVERED_STATUS)
      .map(order => this.getOrder(order.itemOrders));
    const deliveredTotal = deliveredOrders.reduce((acc, value) => value.amount + acc, 0);

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="tiny" closeIcon>
        <Modal.Content className="table-order-details">
          <Header as="h1" textAlign="center">
            Table Details
          </Header>
          <Header as="h2">
            Pending Orders
          </Header>
          {pendingOrders.map((order, index) => {
            const { content, amount } = order;
            return (
              <div className="order-container" key={index}>
                <div className="order-content">{content}</div>
                <div className="order-amount">{amount}</div>
              </div>
            )
          })}
          <div className="filter-total-container">
            <div className="content">Total</div>
            <div className="amount">{pendingTotal}</div>
          </div>
          <Header as="h2">
            Confirmed Orders
          </Header>
          {confirmedOrders.map((order, index) => {
            const { content, amount } = order;
            return (
              <div className="order-container" key={index}>
                <div className="order-content">{content}</div>
                <div className="order-amount">{amount}</div>
              </div>
            )
          })}
          <div className="filter-total-container">
            <div className="content">Total</div>
            <div className="amount">{confirmedTotal}</div>
          </div>
          <Header as="h2">
            Delivered Orders
          </Header>
          {deliveredOrders.map((order, index) => {
            const { content, amount } = order;
            return (
              <div className="order-container" key={index}>
                <div className="order-content">{content}</div>
                <div className="order-amount">{amount}</div>
              </div>
            )
          })}
          <div className="filter-total-container">
            <div className="content">Total</div>
            <div className="amount">{deliveredTotal}</div>
          </div>
          <Header as="h2" className="filter-total-container">
            <span className="content">Total</span>
            <span>{deliveredTotal + confirmedTotal + pendingTotal}</span>
          </Header>
          <Divider/>
          <Button onClick={this.handleClose}>Done</Button>
        </Modal.Content>
      </Modal>
    )
  }
}
