import React, { Component } from 'react';
import { Button, Header, Modal, Segment } from 'semantic-ui-react';
const QRCode = require('qrcode.react');

const getRestaurantWithTablePath = (restaurantId, tableId) => {
  return `http://smartordr.com/restaurant?id=${restaurantId}&tableId=${tableId}`;
}

export default class QRModal extends Component {
  state = {
    modalOpen: false,
  }

  handleOpen = (dimmer) => {
    this.setState({
      dimmer: 'blurring',
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  downloadQR = () => {
    const { table } = this.props;
    const { id } = table;
    const canvas = document.getElementById("qr-code-generator");
    const pngUrl = canvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = `${id}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }

  render() {
    const { modalOpen } = this.state;
    const { restaurant, table } = this.props;
    const trigger = <Button onClick={this.handleOpen}>Show QR</Button>;
    const qrLink = getRestaurantWithTablePath(restaurant.id, table.id);

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="mini" closeIcon>
        <Modal.Content>
          <Header as="h1" textAlign="center">
            QR Code
          </Header>
          <Segment textAlign="center">
            <QRCode id="qr-code-generator" imageSettings={{excavate: true}} value={qrLink} size={250} level="Q" includeMargin={true}/>
          </Segment>
          <div style={{textAlign: "center"}}>
            <Button color="blue" onClick={this.downloadQR}>Download</Button>
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}
