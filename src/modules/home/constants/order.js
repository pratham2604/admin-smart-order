const CONFIRMED = '0';
const DELIVERED = '1';
const NOT_CONFIRMED = '-2';
const CANCELLED = '-1';
const IN_KITCHEN = '2';

export const BOOKING_TYPE_MAP = {
  dine_in: 'Dine In',
  order_pickup: 'Pick Up',
};

export const ORDER_STATUS_MAP = {
  [CONFIRMED]: 'Confirmed',
  [DELIVERED]: 'Delivered',
  [NOT_CONFIRMED]: 'Not Confirmed',
  [CANCELLED]: 'Cancelled',
  [IN_KITCHEN]: 'In Kitchen',
}

export const ORDER_STATUS_COLOR_MAP = {
  [CONFIRMED]: 'yellow',
  [DELIVERED]: 'green',
  [NOT_CONFIRMED]: 'pink',
  [CANCELLED]: 'red',
}

export const NEXT_STEP_MAP = {
  [NOT_CONFIRMED]: CONFIRMED,
  [CONFIRMED]: DELIVERED
}

export const NEXT_STEP_STATUS_MAP = {
  [CONFIRMED]: 'Confirm Order',
  [DELIVERED]: "Deliver Order"
}