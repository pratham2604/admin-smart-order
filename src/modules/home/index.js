import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import PopularItems from './UI/popularItems/index';
import Earnings from './UI/earnings/index';
import Reviews from './UI/reviews/index';
import Pending from './UI/orders/pending';
import Confirmed from './UI/orders/confirmed';
import Delivred from './UI/orders/delivered';
import UserOnboarding from './UI/userOnboarding/index';

export default class extends Component {
  state = {
  }

  render() {
    const { orders, tables, restaurant, addUser, userAdded, updateStatus, payments, reviews } = this.props;
    const { type } = restaurant;

    return (
      <Grid stackable style={{padding: '1em'}}>
        <Grid.Row columns={3}>
          <Grid.Column floated='right'>
            <UserOnboarding tables={tables} type={type} restaurant={restaurant} addUser={addUser} userAdded={userAdded}/>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={3}>
          <Grid.Column>
            <Earnings payments={payments} orders={orders}/>
          </Grid.Column>
          <Grid.Column>
            <PopularItems orders={orders} />
          </Grid.Column>
          <Grid.Column>
            <Reviews reviews={reviews} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={3}>
          <Grid.Column>
            <Pending orders={orders} tables={tables} type={type} updateStatus={updateStatus} restaurant={restaurant}/>
          </Grid.Column>
          <Grid.Column>
            <Confirmed orders={orders} tables={tables} type={type} updateStatus={updateStatus} restaurant={restaurant}/>
          </Grid.Column>
          <Grid.Column>
            <Delivred orders={orders} tables={tables} type={type} restaurant={restaurant}/>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}