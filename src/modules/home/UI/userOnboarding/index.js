import React, { Component, Fragment } from 'react';
import { Button, Header, Modal, Grid } from 'semantic-ui-react';
import FC from '../../../../components/formsy/index';
import { toast } from 'react-toastify';

export default class UserOnboardModal extends Component {
  state = {
    modalOpen: false,
    submitted: false,
  }

  componentDidUpdate(prevProps) {
    const { userAdded } = this.props;
    const { submitted } = this.state;
    if (submitted && (userAdded && !prevProps.userAdded)) {
      this.setState({
        modalOpen: false,
        submitted: false,
      });
      toast.success('User added successfully.')
    }
  }

  handleOpen = () => {
    this.setState({
      dimmer: 'blurring',
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  onSubmit = (model) => {
    const { restaurant = {}, addUser } = this.props;
    const { id } = restaurant;
    const { name, phoneNumber, table } = model;
    const data = Object.assign({} , {
      name,
      phoneNumber,
      restaurantId: id,
      tableId: table.id,
    });
    addUser(data);
    this.setState({
      submitted: true,
    });
  }

  render() {
    const { tables, type } = this.props;
    const { modalOpen, dimmer, canEdit } = this.state;
    const tableType = type && type === 'hotel' ? 'Room' : 'Table';
    const trigger = <Tigger handleOpen={this.handleOpen} />;
    const tableData = tables.map(table => Object.assign({}, {title: table.number, id: table.id}));
    const selectLabel = `Select ${tableType}`;

    return (
      <Modal dimmer={dimmer} trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="tiny" closeIcon>
        <Modal.Content>
          <Header as="h1">
            Add User
          </Header>
          <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit}>
            <Grid stackable columns={2} className="user-onboard-container">
              <Grid.Row>
                <Grid.Column width={16}>
                  <FC.Input fluid placeholder='Name' name="name" required/>
                  <FC.Input fluid placeholder='Phone number' name="phoneNumber" required/>
                  <FC.Search name="table" data={tableData} label={selectLabel}/>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <FC.SubmitButton disabled={!canEdit}>Save</FC.SubmitButton>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </FC.Form>
        </Modal.Content>
      </Modal>
    )
  }
}

const Tigger = (props) => {
  const { handleOpen } = props;
  return (
    <Fragment>
      <Button color="blue" floated="right" onClick={handleOpen}>Add User</Button>
    </Fragment>
  )
}
