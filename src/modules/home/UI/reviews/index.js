import React from 'react';
import { Segment, Header, Divider, Icon, Rating } from 'semantic-ui-react';

export default (props) => {
  const { reviews = [] } = props;
  return (
    <Segment className="earnings">
      <Header as="h2">Reviews</Header>
      <Divider></Divider>
      <div className="reviews-container">
        {reviews.length ?
          <div>
            {reviews.map((review, index) => {
              const { description, rating } = review;
              return (
                <div className="review-container" key={index}>
                  <div className="description">{description}</div>
                  <div className="rating">
                    <Rating rating={rating} icon='star' maxRating={5}/>
                  </div>
                </div>
              )
            })}
          </div> :
          <NoData />
        }
      </div>
    </Segment>
  )
}

const NoData = () => (
  <div className="no-data-container">
    <Icon name="inbox" size="big" color="grey"/>
    <div className="content">No Data</div>
  </div>
)