import React from 'react';
import { Segment, Header, Divider, Button } from 'semantic-ui-react';
import _ from 'lodash';

export default (props) => {
  const { orders = [] } = props;
  const items = [];
  orders.forEach(order => {
    const { itemOrders = [] } = order;
    itemOrders.forEach(item => {
      const { id } = item;
      const target = items.find(itm => itm.id === id);
      if (target) {
        target.count = target.count + 1;
      } else {
        const newEntry = Object.assign({}, item, { count: 1 });
        items.push(newEntry);
      }
    })
  });

  return (
    <Segment className="popular-items">
      <Header as="h2">Popular Items</Header>
      <Divider></Divider>
      <div className="popular-items-container">
        {_.orderBy(items, ['count', 'price'], ['desc', 'desc']).slice(0, 5).map((item, index) => {
          const { name, count, price } = item;
          return (
            <div className="item-row" key={index}>
              <div className="index">{index+1}</div>
              <div className="name">{name}</div>
              <div className="count">{count}</div>
              <div className="amount">{count * price}</div>
            </div>
          )
        })}
        {items.length > 5 &&
          <div className="load-more">
            <Button>Show More</Button>
          </div>
        }
      </div>
    </Segment>
  )
}