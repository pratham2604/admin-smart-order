import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import { Segment, Header, Divider } from 'semantic-ui-react';
import OrderItem from './orderItem';
const CONFIRMED = 0;

export default (props) => {
  const { orders, tables, type, updateStatus, restaurant } = props;
  const status = CONFIRMED;
  const filteredOrder = orders.filter(order => order.statusCode === status);
  const listClass = cx('confirmed-orders-container', {
    'list-container': filteredOrder.length > 5
  });

  return (
    <Segment className="confirmed-orders">
      <Header as="h2">Confirmed Orders</Header>
      <Divider></Divider>
      <div className={listClass}>
        {_.orderBy(filteredOrder, 'recordedAt', 'desc').map(order => <OrderItem tables={tables} order={order} key={order.id} type={type} updateStatus={updateStatus} restaurant={restaurant}/>)}
      </div>
    </Segment>
  )
}