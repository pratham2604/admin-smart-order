import React, { Component } from 'react';
import { Header, Modal, Button, Divider } from 'semantic-ui-react';
import moment from 'moment';
import Receipt from './receiptModal';
import { BOOKING_TYPE_MAP, ORDER_STATUS_MAP, ORDER_STATUS_COLOR_MAP, NEXT_STEP_MAP, NEXT_STEP_STATUS_MAP } from '../../constants/order';

export default class ModalExampleControlled extends Component {
  state = {
    canEdit: false,
    submitted: false,
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  updateStatus = (code) => {
    const { order, updateStatus } = this.props;
    const data = Object.assign({}, order, {statusCode: parseInt(code)});
    updateStatus(data);
    this.props.onClose();
  }

  render() {
    const { order, tableNumber, onClose, type, restaurant } = this.props;
    const { id, itemOrders = [], bookingType, statusCode, recordedAt, chef_note = '' } = order || {};
    const items = [];
    let amount = 0;
    itemOrders.forEach(item => {
      const { customizationsApplied = [] } = item;
      const customizationsAmount = customizationsApplied.reduce((acc, option) => acc + parseInt(option.price), 0);
      amount += (parseInt(item.price) + customizationsAmount);
      const data = Object.assign({}, item);
      const targetItem = items.find(itm => (itm.name === item.name) && customizationsApplied.length === 0 && item.chef_note === itm.chef_note);
      if (targetItem) {
        targetItem.count += 1;
      } else {
        data.count = 1;
        items.push(data);
      }
    });
    const nextStepCode = NEXT_STEP_MAP[statusCode];

    return (
      <Modal dimmer='blurring' open={true} onClose={onClose} centered={false} size="tiny" closeIcon>
        <Modal.Content>
          <div className="order-modal">
            <Header as="h1">
              Order Details
            </Header>
            <div className="order-id">
              <Header as="h3">ID:</Header>
              <div className="content">{id}</div>
            </div>
            <div style={{marginBottom: '1em'}}>Taxes applicable as extra</div>
            <div className="order-id">
              <Header as="h3">Order Time:</Header>
              <div className="content">{moment(recordedAt).format('DD MMM YYYY HH:mm')}</div>
            </div>
            <div className="order-details">
              <Header as="h2">Order:</Header>
              <div className="order-table">
                <div className="order-header">
                  <div className="name">Name</div>
                  <div className="quantity">Quantity</div>
                  <div className="price">Price</div>
                </div>
                {items.map((item, index) => {
                  const { customizationsApplied = [], chef_note } = item;
                  const customizations = customizationsApplied.map(customization => customization.name).join(',');
                  const customizationsAmount = customizationsApplied.reduce((acc, option) => acc + parseInt(option.price), 0);
                  return (
                    <div className="order-row" key={index}>
                      <div className="name">
                        <div>
                          <strong>{item.name}</strong>
                        </div>
                        {customizations && <div>{customizations}</div>}
                        {chef_note &&
                          <div>
                            <b>Note for chef : </b>
                            <i>{chef_note}</i>
                          </div>
                        }
                      </div>
                      <div className="quantity">{item.count}</div>
                      <div className="price">
                        <div>{item.price}</div>
                        {customizations && <div>{customizationsAmount}</div>}
                      </div>
                    </div>
                  )
                })}
                <div className="order-header">
                  <div className="name">Total</div>
                  <div className="price">{amount}</div>
                </div>
              </div>
              <div className="booking-details">
                <Header as="h2">Booking details:</Header>
                <div className="details-container">
                  <div className="title">Booking Type</div>
                  <div className="value">{BOOKING_TYPE_MAP[bookingType]}</div>
                </div>
                {bookingType !== 'order_pickup' &&
                  <div className="details-container">
                    <div className="title">{type} Number</div>
                    <div className="value">{tableNumber}</div>
                  </div>
                }
              </div>
              {chef_note &&
                <div className="booking-details">
                  <Header as="h4">Note for chef:</Header>
                  <div>
                    {chef_note}
                  </div>
                </div>
              }
              <div className="order-details">
                <div className="details-container">
                  <Header as="h3" className="title">Order Status</Header>
                  <Button color={ORDER_STATUS_COLOR_MAP[statusCode]}>{ORDER_STATUS_MAP[statusCode]}</Button>
                </div>
              </div>
            </div>
          </div>
          {nextStepCode && <Divider/>}
          {nextStepCode &&
            <div className="action-container">
              <Button color={ORDER_STATUS_COLOR_MAP[nextStepCode]} onClick={this.updateStatus.bind(this, nextStepCode)}>{NEXT_STEP_STATUS_MAP[nextStepCode]}</Button>
              {statusCode === -2 && <Button color="red" onClick={this.updateStatus.bind(this, '-1')}>Cancel Order</Button>}
            </div>
          }
          <Receipt orderItems={items} order={order} tableNumber={tableNumber} type={type} restaurant={restaurant}/>
        </Modal.Content>
      </Modal>
    )
  }
}