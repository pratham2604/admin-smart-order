import React, { Component } from 'react';
import { Button, Header, Modal } from 'semantic-ui-react';
import OrderItem from './orderItem';
import _ from 'lodash';
const CANCELLED = -1;

export default class AddRoomModal extends Component {
  state = {
    modalOpen: false,
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true,
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
    });
  }

  enableButton = () => {
    this.setState({
      canEdit: true,
    })
  }

  disableButton = () => {
    this.setState({
      canEdit: false,
    });
  }

  render() {
    const { orders, tables, type, updateStatus, restaurant } = this.props;
    const { modalOpen } = this.state;
    const trigger = <Button color="pink" onClick={this.handleOpen}>Cancelled Orders</Button>;
    const filteredOrder = orders.filter(order => order.statusCode === CANCELLED);

    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="mini" closeIcon>
        <Modal.Content>
          <Header as="h1">
            Cancelled Orders
          </Header>
          <div className="pending-orders-container">
            {_.orderBy(filteredOrder, 'recordedAt', 'desc').map(order => <OrderItem tables={tables} order={order} key={order.id} type={type} updateStatus={updateStatus} restaurant={restaurant}/>)}
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}