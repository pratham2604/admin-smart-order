import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import { Segment, Header, Divider } from 'semantic-ui-react';
import OrderItem from './orderItem';
import CancelledOrders from './cancelledOrders';
const NOT_CONFIRMED = -2;

export default (props) => {
  const { orders, tables, type, updateStatus, restaurant } = props;
  const filteredOrder = orders.filter(order => order.statusCode === NOT_CONFIRMED);
  const listClass = cx('pending-orders-container', {
    'list-container': filteredOrder.length > 5
  });

  return (
    <Segment className="pending-orders">
      <Header as="h2">Pending Orders</Header>
      <Divider></Divider>
      <CancelledOrders orders={orders} tables={tables} type={type} updateStatus={updateStatus} restaurant={restaurant}/>
      <Divider></Divider>
      <div className={listClass}>
        {_.orderBy(filteredOrder, 'recordedAt', 'desc').map(order => <OrderItem tables={tables} order={order} key={order.id} type={type} updateStatus={updateStatus} restaurant={restaurant}/>)}
      </div>
    </Segment>
  )
}