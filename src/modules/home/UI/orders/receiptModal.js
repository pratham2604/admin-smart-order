import React, { Component } from 'react';
import { Modal, Button, Divider } from 'semantic-ui-react';
import moment from 'moment';
import ReactToPrint from 'react-to-print';
import DataTable from '../../../../components/UI/datatable/datatable';
import { BOOKING_TYPE_MAP } from '../../constants/order';

const columns = [{
  title: 'Sr',
  field: 'number',
}, {
  title: 'Item',
  field: 'name',
}, {
  title: 'Qty',
  field: 'count',
}, {
  title: 'Rate',
  field: 'price',
}, {
  title: 'Price',
  field: 'amount',
}];

const CGST_AMOUNT = {
  FOOD: 0.025,
  WATER: 0.09,
  LIQUOR: 0.11,
  SOFT_DRINK: 0.06,
};

const SGST_AMOUNT = CGST_AMOUNT;

export default class Receipt extends Component {
  state = {
    modalOpen: false,
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false
    });
  }

  render() {
    const { modalOpen } = this.state;
    const { order, tableNumber, type, restaurant, orderItems } = this.props;
    const { bookingType, number, recordedAt } = order;
    const rows = orderItems.map((item, index) => {
      const { customizationsApplied = [] } = item;
      const customizations = customizationsApplied.map(customization => customization.name).join(',');
      const customizationsAmount = customizationsApplied.reduce((acc, option) => acc + parseInt(option.price), 0);
      const itemPrice = parseInt(item.price) + customizationsAmount;
      return Object.assign({}, {
        number: index + 1,
        name: (
          <div>
            <strong>{item.name}</strong>
            {customizations && <div>{customizations}</div>}
          </div>
        ),
        count: item.count,
        price: itemPrice,
        amount: item.count * itemPrice,
        type: item.type
      });
    });
    const totalAmount = rows.reduce((acc, row) => parseFloat(row.amount) + acc, 0);
    const totalCGST = rows.reduce((acc, row) => {
      const type = row.type || 'FOOD';
      const amount = parseFloat(row.amount) * CGST_AMOUNT[type];
      return amount + acc;
    }, 0);
    const totalSGST = rows.reduce((acc, row) => {
      const type = row.type || 'FOOD';
      const amount = parseFloat(row.amount) * SGST_AMOUNT[type];
      return amount + acc;
    }, 0);
    const trigger = (<Button color="blue" onClick={this.handleOpen} style={{marginTop: '1em'}}>Receipt Preview</Button>);

    const table = { columns, rows };
    return (
      <Modal dimmer="blurring" trigger={trigger} open={modalOpen} onClose={this.handleClose} centered={false} size="small" closeIcon>
        <div style={{padding: '2em', fontFamily: 'Monospace'}} ref={el => (this.componentRef = el)}>
          <div style={{marginBottom: '1em'}}>
            <h3 style={{textAlign: "center", fontWeight: 'bold', marginBottom: '0.5em'}}>INVOICE</h3>
            <h3 style={{textAlign: "center", marginBottom: '0.5em', marginTop: '0'}}>{restaurant.name}</h3>
            <h4 style={{textAlign: "center", marginBottom: '0.5em', marginTop: '0'}}>GST No: {restaurant.gstNo}</h4>
          </div>
          <div>
            <div>Order Number: {('000' + number).slice(-4)}</div>
            <div>Date & Time: {moment(recordedAt).format('DD MMM YYYY HH:mm')}</div>
            <div>{type} Number: {tableNumber}</div>
            <div>Order Type: {BOOKING_TYPE_MAP[bookingType]}</div>
          </div>
          <div>
            <DataTable data={table} />
          </div>
          <div style={{display: 'flex', margin: '1em'}}>
            <div style={{flex: '1', fontWeight: 'bold'}}>Total</div>
            <div style={{fontWeight: 'bold'}}>{totalAmount}</div>
          </div>
          <div style={{display: 'flex', margin: '1em'}}>
            <div style={{flex: '1'}}>Total CGST</div>
            <div>{totalCGST.toFixed(2)}</div>
          </div>
          <div style={{display: 'flex', margin: '1em'}}>
            <div style={{flex: '1'}}>Total SGST</div>
            <div>{totalSGST.toFixed(2)}</div>
          </div>
          <h3 style={{margin: '1em', display: 'flex'}}>
            <div style={{flex: '1', fontWeight: 'bold'}}>Final Amount</div>
            <div style={{fontWeight: 'bold'}}>{(totalAmount + totalCGST + totalSGST).toFixed(2)}</div>
          </h3>
          <Divider/>
          <div>
            Customer Sign:
          </div>
        </div>
        <ReactToPrint
          copyStyles
          trigger={() => <Button color="blue" style={{margin: '1em'}}>Print Receipt</Button>}
          content={() => this.componentRef}
        />
      </Modal>
    )
  }
}