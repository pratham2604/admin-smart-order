import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import { Segment, Header, Divider } from 'semantic-ui-react';
import OrderItem from './orderItem';
const DELIVERED = 1;

export default (props) => {
  const { orders, tables, type, restaurant } = props;
  const status = DELIVERED;
  const filteredOrder = orders.filter(order => order.statusCode === status);
  const listClass = cx('delivered-orders-container', {
    'list-container': filteredOrder.length > 5
  });

  return (
    <Segment className="delivered-orders">
      <Header as="h2">Delivered Orders</Header>
      <Divider></Divider>
      <div className={listClass}>
        {_.orderBy(filteredOrder, 'recordedAt', 'desc').map(order => <OrderItem tables={tables} order={order} key={order.id} type={type} restaurant={restaurant}/>)}
      </div>
    </Segment>
  )
}