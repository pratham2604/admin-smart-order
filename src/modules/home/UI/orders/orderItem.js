import React, { Fragment, Component } from 'react';
import OrderModal from './orderModal';

export default class extends Component {
  state = {
    showModal: false,
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal,
    });
  }

  render() {
    const { showModal } = this.state;
    const { order, tables, type, updateStatus, restaurant } = this.props;
    const { id, tableId, itemOrders = [], bookingType } = order;
    const table = tables.find(tableItem => tableItem.id === tableId) || {};
    const tableType = type && type === 'hotel' ? 'Room' : 'Table';
    const tableNumber = table.number ? `${tableType} ${table.number}` : `Unknown ${tableType}`;
    const dishes = [];
    let amount = 0;
    itemOrders.forEach(itemOrder => {
      const { name, price, customizationsApplied = [] } = itemOrder || {};
      const dishExist = dishes.find(dish => dish === name);
      if (!dishExist) {
        dishes.push(name);
      }
      const customizationAmount = customizationsApplied.reduce((acc, option) => acc + parseInt(option.price), 0);
      const itemAmount = parseInt(price) + customizationAmount;
      amount += itemAmount;
    });
    const dishesContent = dishes.join(',');

    return (
      <Fragment>
        <div key={id} className="order-item" onClick={this.toggleModal}>
          <div className="title">
            {bookingType === 'order_pickup' ? 'Pick up' : tableNumber}
          </div>
          <div className="order-content">
            <div className="dishes">{dishesContent}</div>
            <div className="amount">{amount}</div>
          </div>
        </div>
        {showModal && <OrderModal order={order} onClose={this.toggleModal} tableNumber={tableNumber} type={tableType} updateStatus={updateStatus} restaurant={restaurant}/>}
      </Fragment>
    )
  }
}