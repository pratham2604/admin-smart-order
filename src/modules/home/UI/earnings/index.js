import React from 'react';
import { Segment, Header, Divider, Icon } from 'semantic-ui-react';
import moment from 'moment';
import _ from 'lodash';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';

export default (props) => {
  const { payments = [], orders = [] } = props;
  const paymentsData = payments.filter(payment => {
    const targetOrder = orders.find(order => order.id === payment.orderId) || {};
    return targetOrder.statusCode === 1;
  }).map(payment => {
    const { recordedAt, totalAmount } = payment;
    const targetOrder = orders.find(order => order.id === payment.orderId) || {};
    const date = moment(recordedAt).format('DD MMM YYYY');
    return {
      date,
      amount: totalAmount,
      statusCode: targetOrder.statusCode
    };
  });
  const chartData = [];
  paymentsData.forEach(payment => {
    const { date, amount } = payment;
    const targetDate = chartData.find(data => data.date === date);
    if (targetDate) {
      targetDate.amount += amount;
    } else {
      chartData.push(payment);
    }
  });
  const sortedChartData = _.orderBy(chartData.map(data => {
    return Object.assign({}, data, {
      dateTime: new Date(data.date),
    });
  }), 'dateTime', 'asc');

  return (
    <Segment className="earnings">
      <Header as="h2">Earnings</Header>
      <Divider></Divider>
      <div className="earnings-container">
        {sortedChartData.length ?
          <ResponsiveContainer>
            <LineChart data={sortedChartData} margin={{ top: 5, right: 30, left: 0, bottom: 5, }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="date" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="amount" stroke="#82ca9d" />
            </LineChart>
          </ResponsiveContainer> :
          <NoData />
        }
      </div>
    </Segment>
  )
}

const NoData = () => (
  <div className="no-data-container">
    <Icon name="inbox" size="big" color="grey"/>
    <div className="content">No Data</div>
  </div>
)