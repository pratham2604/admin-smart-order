import React, { Component } from 'react'
import { Grid, Header, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import FC from '../../../components/formsy/index';
import { toast } from 'react-toastify';
import COUNTRIES from '../../../lib/countries';

class SignupForm extends Component {
  state = {
    canSubmit: false,
    country: ''
  }

  validations = {
    email: [{
      validator: 'isRequired',
    }, {
      validator: 'isEmail',
    }], 
    password: [{
      validator: 'isRequired',
    }, {
      validator: 'isPassword',
    }],
    name: [{
      validator: 'isRequired',
    }],
    description: [{
      validator: 'isRequired',
    }],
    phoneNumber: [{
      validator: 'isRequired',
    }],
  }

  enableButton = () => {
    this.setState({
      canSubmit: true,
    });
  }

  disableButton = () => {
    this.setState({
      canSubmit: false,
    });
  }

  onCountrySelect = (name, value) => {
    this.setState({
      country: (value && value.id) || 'IN'
    })
  }

  onSubmit = (model) => {
    const { onSignUp } = this.props;
    const data = Object.assign({}, model);
    const { image } = model;
    delete data.image;
    data.fileName = image && image.fileName || '';
    data.thumb = image && image.link || '';
    data.photos = [data.thumb];
    data.location = {};
    onSignUp(data);
  }

  render() {
    const { canSubmit, country } = this.state;
    const { loading } = this.props;
    const types = [{
      text: 'Restaurant',
      value: 'restaurant',
    }, {
      text: 'Hotel',
      value: 'hotel'
    }, {
      text: 'Shop',
      value: 'shop',
    }, {
      text: 'Services',
      value: 'services'
    }, {
      text: 'Others',
      value: 'others',
    }];

    const CURRENCY_OPTIONS = [{
      text: 'Rupee',
      value: 'INR',
    }, {
      text: 'Dollar',
      value: 'DOLLAR',
    }, {
      text: 'Euro',
      value: 'EURO',
    }, {
      text: 'Pound',
      value: 'POUND',
    }];
    const countriesOptions = COUNTRIES.map(country => Object.assign({}, { title: country.name, id: country.id }));

    return (
      <Grid textAlign='center' className="auth-form" verticalAlign='middle'>
        <Grid.Column mobile={16} tablet={12} computer={8} style={{paddingRight: 0}}>
          <Segment raised className="form-container">
            <Header as='h2' className="form-header" textAlign='center'>
              Create your account
            </Header>
            <Header as='h5' className="description">
              Fill the form below with your business details to setup the management system
            </Header>
            <FC.Form onValid={this.enableButton} onInvalid={this.disableButton} onSubmit={this.onSubmit} validations={this.validations}>
              <FC.Input fluid icon='user' iconPosition='left' placeholder='Business Name' name="name" formValidations={this.validations.name} required/>
              <FC.Input fluid icon='mail' iconPosition='left' placeholder='E-mail address' name="email" formValidations={this.validations.email} required/>
              <FC.Input fluid icon='lock' iconPosition='left' placeholder='Password' name="password" type="password" formValidations={this.validations.password} required/>
              <FC.Input fluid placeholder='Business Description' name="description" formValidations={this.validations.description} required/>
              <div className="input-example">e.g. "coffee shop, bar, fine dine restaurant, etc."</div>
              <FC.PhoneInput vertical label='Phone Number' name="phoneNumber" formValidations={this.validations.phoneNumber} required/>
              <FC.Search vertical fluid label="Select Country" name="country" required data={countriesOptions} onSelect={this.onCountrySelect}/>
              {country === 'IN' ?
                <FC.Input fluid placeholder='GST No (Optional)' name="gstNo"/> :
                <FC.Input fluid placeholder='Business Registration No (Optional)' name="registrationNo"/>
              }
              <FC.ImageUpload name="image" label="Business Display Picture:" />
              <FC.Select vertical fluid placeholder='Type' label="Select Business Type" name="type" required options={types} />
              <FC.Select vertical fluid placeholder='Currency' label="Select Currency" name="currency" required options={CURRENCY_OPTIONS} />
              <FC.Checkbox vertical fluid label="Home Deliveries" name="delivery" optional toggle />
              <FC.SubmitButton loading={loading} disabled={!canSubmit} className="submit-button" size='large' type="submit">Register</FC.SubmitButton>
            </FC.Form>
            <div className="form-footer">
              Already have account? <Link to="/login">Log in</Link>
            </div>
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }
}

export default SignupForm;