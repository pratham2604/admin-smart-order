import React, { Component, Fragment } from 'react';
import { withFormsy } from 'formsy-react';
import cx from 'classnames';
import { toast } from 'react-toastify';
import { Grid, Form, Image, Button, Header, Segment, Dimmer, Loader } from 'semantic-ui-react';
import ImageHolder from '../../UI/imageHolder/imageHolder';
// import Loader from './../../UI/loader/loader';
import { uploadDoc, deleteDoc } from '../../../actions/storageHandler';

const maxSize = 5*1000*1000;

class ImageUploader extends Component {
  state = {
    uploading: false,
    deleting: false,
    error: ''
  };

  onUploadClick = () => {
    const { fileInput } = this.refs;
    fileInput.click();
  }

  uploadFile() {
    const { file } = this.state;
    if (file.size > maxSize) {
      const error = 'Max file size exceeded !!';
      toast.error(error);
      this.setState({
        error,
      })
      return;
    }

    this.setState({
      uploading: true,
    });
    uploadDoc(file, this.onSuccess, this.onFailure);
  }

  onSuccess = (data) => {
    const { onUpload, setValue, name } = this.props;
    this.setState({
      uploading: false,
    });
    toast.success('File uploaded successfully.');
    setValue(data);
    onUpload && onUpload(name, data);
  }

  onFailure = (error) => {
    this.setState({
      uploading: false,
    });
    toast.error(error);
    this.setState({
      error,
    });
  }

  _handleImageChange = (event) => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
      });
      this.uploadFile();
    };

    // To clear currently loaded file from input
    event.target.value = null;
    reader.readAsDataURL(file);
  }

  onDeleteSuccess = () => {
    this.setState({
      deleting: false,
    });
    toast.success('File deleted successfully.');
    const data = {
      fileName: '',
      link: ''
    };
    this.props.setValue(data);
  }

  onDeleteFailure = (error) => {
    this.setState({
      deleting: false,
    });
    toast.error(error);
    const { refetchData } = this.props;
    refetchData && refetchData();
  }

  onDeleteDoc = () => {
    this.setState({
      deleting: true,
    });
    const { fileName } = this.props.value || {};
    deleteDoc(fileName, this.onDeleteSuccess, this.onDeleteFailure);
  }

  render() {
    const { uploading, deleting } = this.state;
    const { label, className, value, mobile = 16, tablet = 8, computer = 4, width } = this.props;
    const { link, fileName } = value || {};
    const formClass = cx('form-group', className);

    return (
      <Segment>
        <Form.Field>
          <Header as="h5" textAlign="left">{`Upload ${label}:`}</Header>
          <Grid textAlign="left" className={formClass}>
            {!uploading && link ?
              <Fragment>
                {deleting ?
                  <ImageLoader /> :
                  <Fragment>
                    <Grid.Column mobile={mobile} tablet={tablet} computer={computer} verticalAlign="middle">
                      <div className="upload-doc-button" onClick={this.onUploadClick}>
                        <Image src={link} size="small" bordered rounded style={{width}}/>
                        <input type="file" ref="fileInput" className="file-input" onChange={this._handleImageChange}></input>
                      </div>
                    </Grid.Column>
                    <Grid.Column mobile={mobile} tablet={tablet} computer={computer} verticalAlign="middle">
                      <Button.Group>
                        <Button as="a" href={link} target="_blank" icon="download"></Button>
                        {fileName && <Button icon="delete" onClick={this.onDeleteDoc}></Button>}
                      </Button.Group>
                    </Grid.Column>
                  </Fragment>
                }
              </Fragment> :
              <Grid.Column mobile={mobile} tablet={tablet} computer={computer} verticalAlign="middle">
                {uploading ?
                  <ImageLoader /> :
                  <div className="upload-doc-button" onClick={this.onUploadClick}>
                    <ImageHolder size="small" style={{width}}/>
                    <input type="file" ref="fileInput" className="file-input" onChange={this._handleImageChange}></input>
                  </div>
                }
              </Grid.Column>
            }
          </Grid>
        </Form.Field>
      </Segment>
    );
  }
}

const ImageLoader = () => (
  <div style={{height: '8em'}}>
    <Dimmer active inverted>
      <Loader inverted active />
    </Dimmer>
  </div>
)

export default withFormsy(ImageUploader);
