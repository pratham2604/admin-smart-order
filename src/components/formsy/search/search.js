import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import _ from 'lodash';
import { Search, Form, Grid, Header } from 'semantic-ui-react';
const initialState = { isLoading: false, results: [], search: '' }

class FormsySearch extends Component {
  state = initialState

  onSelect = (e, { result }) => {
    const { onSelect, name, setValue } = this.props;
    setValue(result);
    this.setState({
      search: result.title,
    });
    onSelect && onSelect(name, result);
  }

  handleSearchChange = (e, { value }) => {
    const { data } = this.props;
    this.setState({ isLoading: true, search: value })

    setTimeout(() => {
      if (this.state.search.length < 1) {
        return this.setState(initialState)
      }

      const re = new RegExp(_.escapeRegExp(this.state.search), 'i');
      const isMatch = (result) => re.test(result.title)

      this.setState({
        isLoading: false,
        results: _.filter(data, isMatch),
      })
    }, 300)
  }

  render() {
    const { label, vertical = false } = this.props;
    const { isLoading, results, search } = this.state;
    const labelWidth = vertical ? 16 : 4;
    const inputWidth = vertical ? 4 : 4;

    return (
      <Form.Field>
        <Grid textAlign="left">
          <Grid.Column mobile={16} tablet={16} computer={labelWidth} verticalAlign="middle" style={{paddingBottom: 0}}>
            <Header as="h5">{label}</Header>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={inputWidth}>
            <Search
              loading={isLoading}
              onResultSelect={this.onSelect}
              onSearchChange={_.debounce(this.handleSearchChange, 500, {
                leading: true,
              })}
              results={results}
              value={search}
            />
          </Grid.Column>
        </Grid>
      </Form.Field>
    );
  }
}

export default withFormsy(FormsySearch);
