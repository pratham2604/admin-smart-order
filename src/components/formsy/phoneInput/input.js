import React, { Component } from 'react';
import PhoneInput from 'react-phone-input-2'
import { withFormsy } from 'formsy-react';
import { Form, Grid, Header } from 'semantic-ui-react';
import { resetInputValidation } from '../helper/index';
import 'react-phone-input-2/lib/style.css';

class FormsyInput extends Component {
  state = {
    isValueInValid: false,
    error: '',
  };

  componentDidMount() {
    const { value, setValue } = this.props;
    value && setValue(value || '');
  }

  onChange = (value) => {
    const { onChange, name } = this.props;

    this.setState({
      isValueInValid: false,
      error: '',
    });

    resetInputValidation(this.props);
    this.props.setValue(value);
    onChange && onChange(name, value);
  }

  render() {
    const { value, vertical = false } = this.props;
    const labelWidth = vertical ? 16 : 4;
    const inputWidth = vertical ? 8 : 12;

    return (
      <Form.Field>
        <div className="custon-phone-input">
          <Grid stackable>
            <Grid.Row>
              <Grid.Column width={labelWidth}>
                <Header as="h5" className="phone-input-label">Phone Number</Header>
              </Grid.Column>
              <Grid.Column width={inputWidth}>
                <PhoneInput
                  country={'in'}
                  value={value}
                  onChange={this.onChange}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Form.Field>
      
    );
  }
}

export default withFormsy(FormsyInput);
