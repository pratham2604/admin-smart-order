import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import { Dropdown, Form, Grid, Header } from 'semantic-ui-react';

class FormsySearch extends Component {
  state = {
  }

  componentDidMount() {
    const { value, setValue } = this.props;
    value && setValue(value || '');
  }

  onChange = (e, { value }) => {
    const { setValue, name, onChange } = this.props;
    value && setValue(value || '');
    onChange && onChange(name, value);
  }

  render() {
    const { label, options, placeholder, value, vertical = false } = this.props;
    const labelWidth = vertical ? 16 : 4;
    const inputWidth = vertical ? 4 : 4;

    return (
      <Form.Field>
        <Grid textAlign="left">
          <Grid.Column mobile={16} tablet={8} computer={labelWidth} verticalAlign="middle" style={{paddingBottom: 0}}>
            <Header as="h5">{label}</Header>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={inputWidth}>
            <Dropdown selection options={options} placeholder={placeholder} onChange={this.onChange} value={value}/>
          </Grid.Column>
        </Grid>
      </Form.Field>
    );
  }
}

export default withFormsy(FormsySearch);
