import React from 'react';
import { Button } from 'semantic-ui-react';

export default (props) => {
  const { id = 'action-button', type, disabled, className, loading, children, onClick, ...rest } = props;
  return (
      <Button
        type={type}
        className={className}
        disabled={disabled || loading}
        id={id}
        onClick={onClick}
        loading={loading}
        {...rest}
      >
        {children}
      </Button>
  );
}
