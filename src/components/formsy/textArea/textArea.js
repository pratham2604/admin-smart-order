import React, { Component, Fragment } from 'react';
import { withFormsy } from 'formsy-react';
import { TextArea } from 'semantic-ui-react';
import cx from 'classnames';
import { getFormError, resetInputValidation, validateForError } from '../helper/index';

class FormsyTextArea extends Component {
  state = {
    isValueInValid: false,
    error: '',
  };

  componentDidMount() {
    const { value, setValue } = this.props;
    value && setValue(value || '');
  }

  onChange = (event) => {
    const { name, value } = event.currentTarget;
    const { onChange } = this.props;

    this.setState({
      isValueInValid: false,
      error: '',
    });

    resetInputValidation(this.props);
    this.props.setValue(value);
    onChange && onChange(name, value);
  }

  onBlur = (event) => {
    const { name, value } = event.currentTarget;
    const { onBlur } = this.props;
    const error = validateForError(this.props, value);

    if (error) {
      this.setState({
        isValueInValid: true,
        error,
      });
    }

    onBlur && onBlur(name, value);
  }

  render() {
    const { name, placeholder, defaultValue, formValidations, className, value } = this.props;
    const { error } = this.state;
    const formError = getFormError(this.props, formValidations);
    const formClass = cx('formsy-textarea', className);

    return (
      <Fragment>
        <TextArea
          className={formClass}
          name={name}
          placeholder={placeholder}
          onChange={this.onChange}
          onBlur={this.onBlur}
          value={value || defaultValue || ''}
        />
        {error && <div className="error">{error || formError}</div>}
      </Fragment>
    );
  }
}

export default withFormsy(FormsyTextArea);
