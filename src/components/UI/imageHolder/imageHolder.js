import React from 'react';
import { Image } from 'semantic-ui-react';
import ImagePlaceHolder from '../../../assets/imagePlaceHolder.png';

export default (props) => {
  const { src, ...rest } = props;
  const imageSource = src || ImagePlaceHolder;

  return (
    <Image {...rest} src={imageSource} />
  )
}