import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';

let fetchMenuSubscription = null;

export const fetchMenuItems = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_MENU_REQUEST});
    fetchMenuSubscription = Firestore.collection("restaurant_menu").doc(restaurantId).onSnapshot(snapshot => {
      if (snapshot.data()) {
        const menuData = snapshot.data();
        const { items, id } = menuData;
        dispatch({type: TYPES.FETCH_MENU_SUCCESS, data: items, menuId: id});
        return;
      } else {
        dispatch({type: TYPES.FETCH_MENU_SUCCESS, data: [], menuId: null});
      }
    });
  }
}

export const unsubscribeMenu = () => {
  fetchMenuSubscription();
}

export const updateMenu = (data, action) => {
  return (dispatch) => {
    dispatch({type: TYPES.ADD_MENU_REQUEST});
    Firestore.collection("restaurant_menu").doc(data.restaurantId)[action](data)
    .then((snapshot) => {
      dispatch({type: TYPES.ADD_MENU_SUCCESS});
    }).catch((error) => {
      toast.error(error);
    });
  }
}

let fetchCategorySubscription = null;

export const fetchFoodCategories = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_MENU_CATEGORIES_REQUEST});
    fetchCategorySubscription = Firestore.collection("food_categories").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_MENU_CATEGORIES_SUCCESS, data});
    });
  }
}

export const unsubscribeFoodCategories = () => {
  fetchCategorySubscription();
}

export const updateCategory = (data, action) => {
  return (dispatch) => {
    Firestore.collection("food_categories").doc(data.id)[action](data)
    .then((snapshot) => {
      // console.log(snapshot);
    }).catch((error) => {
      toast.error(error);
    });
  }
}

export const deleteCategory = (data) => {
  return (dispatch) => {
    Firestore.collection("food_categories").doc(data.id).delete()
    .then((snapshot) => {
      // console.log(snapshot);
    }).catch((error) => {
      toast.error(error);
    });
  }
}