import { FirebaseStorage } from '../lib/firebase';
import { v4 as uuidv4 } from 'uuid';

export const uploadDoc = (file, onSuccess, onFailure) => {
  const fileName = uuidv4();
  FirebaseStorage.ref(fileName).put(file).then(snapshot => {
    snapshot.ref.getDownloadURL().then(link => {
      const data = { fileName, link };
      onSuccess(data);
    })
  }).catch(err => {
    onFailure(err);
  });
}

export const deleteDoc = (fileName, onSuccess, onFailure) => {
  FirebaseStorage.ref(fileName).delete().then(snapshot => {
    onSuccess();
  }).catch(err => {
    onFailure(err);
  });
}